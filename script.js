document.getElementById('IpBtn').addEventListener('click', getData);

async function getData() {
    try {
        const Response = await fetch('https://api.ipify.org/?format=json');
        console.log(Response);
        const ipData = await Response.json();
        console.log(ipData);
        const ipResult = ipData.ip;
        console.log(ipResult);

        const locationResponse = await fetch(`http://ip-api.com/json/${ipResult}`);
        const locationData = await locationResponse.json();
        console.log(locationData);

        const result = document.getElementById('result');
        result.innerHTML = `
            <h4>Інформація, що отримана з останнього запиту за IP ${ipResult}:</h4>
            <ul>
            <li>Континент: ${locationData.timezone}</li>
            <li>Країна: ${locationData.country}</li>
            <li>Регіон: ${locationData.region}</li>
            <li>Місто: ${locationData.city}</li>
            <li>Район: ${locationData.zip}</li>
            </ul>
        `;
    
    } catch (error) {
        console.error('Знайдена помилка =>', error);
    }
}
